package arquivoRetornoDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroHeaderPedido struct {
	tipoDoRegistro                 int32    	`json:"tipoDoRegistro"`
	cnpjDoCliente                  string 	`json:"cnpjDoCliente"`
	numeroDoPedido                 string 	`json:"numeroDoPedido"`
	numeroDoPedidoDistribuidor     string 	`json:"numeroDoPedidoDistribuidor"`
	dataDeGeracaoDeRetorno         int32    	`json:"dataDeGeracaoDeRetorno"`
	horaDeGeracaoDoPedido          int32    	`json:"horaDeGeracaoDoPedido"`
	valorAtendido                  float32	`json:"valorAtendido"`
	posicaoDoPedido                int32  	`json:"posicaoDoPedido"`
	codigoDoMotivoDeNaoAtendimento int32  	`json:"codigoDoMotivoDeNaoAtendimento"`
}

func (r *RegistroHeaderPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroHeaderPedido

	err = posicaoParaValor.ReturnByType(&r.tipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedidoDistribuidor, "numeroDoPedidoDistribuidor")
	err = posicaoParaValor.ReturnByType(&r.dataDeGeracaoDeRetorno, "dataDeGeracaoDeRetorno")
	err = posicaoParaValor.ReturnByType(&r.horaDeGeracaoDoPedido, "horaDeGeracaoDoPedido")
	err = posicaoParaValor.ReturnByType(&r.valorAtendido, "valorAtendido")
	err = posicaoParaValor.ReturnByType(&r.posicaoDoPedido, "posicaoDoPedido")
	err = posicaoParaValor.ReturnByType(&r.codigoDoMotivoDeNaoAtendimento, "codigoDoMotivoDeNaoAtendimento")

	return err
}

var PosicoesRegistroHeaderPedido = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":                 {0, 2, 0},
	"cnpjDoCliente":                  {3, 17, 0},
	"numeroDoPedido":                 {18, 30, 0},
	"numeroDoPedidoDistribuidor":     {31, 43, 0},
	"dataDeGeracaoDeRetorno":         {44, 52, 0},
	"horaDeGeracaoDoPedido":          {53, 61, 0},
	"valorAtendido":                  {62, 74, 2},
	"posicaoDoPedido":                {75, 77, 0},
	"codigoDoMotivoDeNaoAtendimento": {78, 80, 0},
}