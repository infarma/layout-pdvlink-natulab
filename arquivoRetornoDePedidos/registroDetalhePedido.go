package arquivoRetornoDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhePedido struct {
	tipoDoRegistro                 int32  `json:"tipoDoRegistro"`
	cnpjDoCliente                  string `json:"cnpjDoCliente"`
	numeroDoPedido                 string `json:"numeroDoPedido"`
	sequenciaDoItem                int32  `json:"sequenciaDoItem"`
	codigoDoProduto                int32  `json:"codigoDoProduto"`
	quatidadeAtendida              int32  `json:"quatidadeAtendida"`
	posicaoDoItemDoPedido          int32  `json:"posicaoDoItemDoPedido"`
	codigoDoMotivoDeNaoAtendimento int32  `json:"codigoDoMotivoDeNaoAtendimento"`
}

func (r *RegistroDetalhePedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhePedido

	err = posicaoParaValor.ReturnByType(&r.tipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.sequenciaDoItem, "sequenciaDoItem")
	err = posicaoParaValor.ReturnByType(&r.codigoDoProduto, "codigoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.quatidadeAtendida, "quatidadeAtendida")
	err = posicaoParaValor.ReturnByType(&r.posicaoDoItemDoPedido, "posicaoDoItemDoPedido")
	err = posicaoParaValor.ReturnByType(&r.codigoDoMotivoDeNaoAtendimento, "codigoDoMotivoDeNaoAtendimento")

	return err
}

var PosicoesRegistroDetalhePedido = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":                 {0, 2, 0},
	"cnpjDoCliente":                  {3, 17, 0},
	"numeroDoPedido":                 {18, 30, 0},
	"sequenciaDoItem":                {31, 36, 0},
	"codigoDoProduto":                {37, 50, 0},
	"quatidadeAtendida":              {51, 57, 0},
	"desconto":                       {58, 63, 2},
	"codigoDoMotivoDeNaoAtendimento": {64, 76, 2},
}