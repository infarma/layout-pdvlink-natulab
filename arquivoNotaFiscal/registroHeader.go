package arquivoNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroHeader struct {
	tipoDoRegistro               int32    	`json:"tipoDoRegistro"`
	cnpjDoCliente                string 	`json:"cnpjDoCliente"`
	numeroDaNotaFiscal           string 	`json:"numeroDaNotaFiscal"`
	serieDaNotaFiscal            string 	`json:"serieDaNotaFiscal"`
	numeroDoPedido               string 	`json:"numeroDoPedido"`
	numeroDoPedidoDoDistribuidor string 	`json:"numeroDoPedidoDoDistribuidor"`
	dataDeFaturamento            int32 	`json:"dataDeFaturamento"`
	horaDeFaturamento            int32  	`json:"horaDeFaturamento"`
	tipoDaNotaFiscal             int32    	`json:"tipoDaNotaFiscal"`
	totalFaturado                float32	`json:"totalFaturado"`
	valorST                      float32	`json:"valorST"`
	valorICMS                    float32	`json:"valorICMS"`
	baseST                       float32	`json:"baseST"`
	baseICMS                     float32	`json:"baseICMS"`
	valorSUFRAMA                 float32	`json:"valorSUFRAMA"`
}

func (r *RegistroHeader) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroHeader

	err = posicaoParaValor.ReturnByType(&r.tipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.numeroDaNotaFiscal, "numeroDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.serieDaNotaFiscal, "serieDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedidoDoDistribuidor, "numeroDoPedidoDoDistribuidor")
	err = posicaoParaValor.ReturnByType(&r.dataDeFaturamento, "dataDeFaturamento")
	err = posicaoParaValor.ReturnByType(&r.horaDeFaturamento, "horaDeFaturamento")
	err = posicaoParaValor.ReturnByType(&r.tipoDaNotaFiscal, "tipoDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.totalFaturado, "totalFaturado")
	err = posicaoParaValor.ReturnByType(&r.valorST, "valorST")
	err = posicaoParaValor.ReturnByType(&r.valorICMS, "valorICMS")
	err = posicaoParaValor.ReturnByType(&r.baseST, "baseST")
	err = posicaoParaValor.ReturnByType(&r.baseICMS, "baseICMS")
	err = posicaoParaValor.ReturnByType(&r.valorSUFRAMA, "valorSUFRAMA")

	return err
}

var PosicoesRegistroHeader = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":                      {0, 2, 0},
	"cnpjDoCliente":                      {3, 17, 0},
	"numeroDaNotaFiscal":                      {18, 28, 0},
	"serieDaNotaFiscal":                      {29, 34, 0},
	"numeroDoPedido":                      {35, 47, 0},
	"numeroDoPedidoDoDistribuidor":                      {48, 60, 0},
	"dataDeFaturamento":                      {61, 69, 0},
	"horaDeFaturamento":                      {70, 78, 0},
	"tipoDaNotaFiscal":                      {79, 81, 0},
	"totalFaturado":                      {82, 94, 2},
	"valorST":                      {95, 107, 2},
	"valorICMS":                      {108, 120, 2},
	"baseST":                      {121, 133, 2},
	"baseICMS":                      {134, 146, 2},
	"valorSUFRAMA":                      {147, 159, 2},
}