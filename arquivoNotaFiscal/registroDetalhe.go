package arquivoNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	tipoDoResgistro              int32    	`json:"tipoDoResgistro"`
	cnpjDoCliente                string 	`json:"cnpjDoCliente"`
	numeroDaNotaFiscal           string 	`json:"numeroDaNotaFiscal"`
	serieDaNotaFiscal            string 	`json:"serieDaNotaFiscal"`
	numeroDoPedido               string 	`json:"numeroDoPedido"`
	numeroDoPedidoDoDistribuidor string 	`json:"numeroDoPedidoDoDistribuidor"`
	sequenciaDoItem              int32    	`json:"sequenciaDoItem"`
	codigoDoProduto              int32    	`json:"codigoDoProduto"`
	quantidadeFaturada           int32    	`json:"quantidadeFaturada"`
	valorUnitarioDoItem          float32	`json:"valorUnitarioDoItem"`
	valorTotalDoItem             float32	`json:"valorTotalDoItem"`
	baseST                       float32	`json:"baseST"`
	valorST                      float32	`json:"valorST"`
	baseICMS                     float32	`json:"baseICMS"`
	valorICMS                    float32	`json:"valorICMS"`
	valorSUFRAMA                 float32	`json:"valorSUFRAMA"`
	loteDeMedicamento            string 	`json:"loteDeMedicamento"`
	chaveDeAcesso                string 	`json:"chaveDeAcesso"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.tipoDoResgistro, "tipoDoResgistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.numeroDaNotaFiscal, "numeroDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.serieDaNotaFiscal, "serieDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedidoDoDistribuidor, "numeroDoPedidoDoDistribuidor")
	err = posicaoParaValor.ReturnByType(&r.sequenciaDoItem, "sequenciaDoItem")
	err = posicaoParaValor.ReturnByType(&r.codigoDoProduto, "codigoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.quantidadeFaturada, "quantidadeFaturada")
	err = posicaoParaValor.ReturnByType(&r.valorUnitarioDoItem, "valorUnitarioDoItem")
	err = posicaoParaValor.ReturnByType(&r.valorTotalDoItem, "valorTotalDoItem")
	err = posicaoParaValor.ReturnByType(&r.baseST, "baseST")
	err = posicaoParaValor.ReturnByType(&r.valorST, "valorST")
	err = posicaoParaValor.ReturnByType(&r.baseICMS, "baseICMS")
	err = posicaoParaValor.ReturnByType(&r.valorICMS, "valorICMS")
	err = posicaoParaValor.ReturnByType(&r.valorSUFRAMA, "valorSUFRAMA")
	err = posicaoParaValor.ReturnByType(&r.loteDeMedicamento, "loteDeMedicamento")
	err = posicaoParaValor.ReturnByType(&r.chaveDeAcesso, "chaveDeAcesso")

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoResgistro":              {0, 2, 0},
	"cnpjDoCliente":                {3, 17, 0},
	"numeroDaNotaFiscal":           {18, 28, 0},
	"serieDaNotaFiscal":            {29, 34, 0},
	"numeroDoPedido":               {35, 47, 0},
	"numeroDoPedidoDoDistribuidor": {48, 60, 0},
	"sequenciaDoItem":              {61, 66, 0},
	"codigoDoProduto":              {67, 80, 0},
	"quantidadeFaturada":           {81, 87, 0},
	"valorUnitarioDoItem":          {88, 100, 2},
	"valorTotalDoItem":             {101, 113, 2},
	"baseST":                       {114, 126, 2},
	"valorST":                      {127, 139, 2},
	"baseICMS":                     {140, 152, 2},
	"valorICMS":                    {153, 165, 2},
	"valorSUFRAMA":                 {166, 178, 2},
	"loteDeMedicamento":            {179, 199, 0},
	"chaveDeAcesso":                {200, 244, 0},
}