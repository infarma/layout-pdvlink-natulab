package arquivoEstoque

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	tipoDeRegistro          int32 `json:"tipoDeRegistro"`
	codigoDeBarrasDoProduto int32 `json:"codigoDeBarrasDoProduto"`
	quantidadeDeEstoque     int32 `json:"quantidadeDeEstoque"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.tipoDeRegistro, "tipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.codigoDeBarrasDoProduto, "codigoDeBarrasDoProduto")
	err = posicaoParaValor.ReturnByType(&r.quantidadeDeEstoque, "quantidadeDeEstoque")

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDeRegistro":          {0, 2,0},
	"codigoDeBarrasDoProduto": {3, 16,0},
	"quantidadeDeEstoque":     {17, 26,0},
}