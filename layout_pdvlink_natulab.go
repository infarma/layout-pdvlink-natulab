package layout_pdvlink_natulab

import (
	"bitbucket.org/infarma/layout-pdvlink-natulab/arquivoRemessaDePedidos"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoRemessaDePedidos.ArquivoDePedido, error) {
	return arquivoRemessaDePedidos.GetStruct(fileHandle)
}
