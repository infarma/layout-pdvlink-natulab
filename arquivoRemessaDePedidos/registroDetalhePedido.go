package arquivoRemessaDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhePedido struct {
	TipoDoRegistro      int32   `json:"TipoDoRegistro"`
	CnpjDoCliente       string  `json:"CnpjDoCliente"`
	NumeroDoPedido      string  `json:"NumeroDoPedido"`
	SequenciaDoItem     int32   `json:"SequenciaDoItem"`
	CodigoDoProduto     int32   `json:"CodigoDoProduto"`
	Quatidade           int32   `json:"Quatidade"`
	Desconto            float32 `json:"Desconto"`
	ValorUnitarioDoItem float32 `json:"ValorUnitarioDoItem"`
	ValorTotalDoItem    float32 `json:"ValorTotalDoItem"`
	TipoDoItem          int     `json:"TipoDoItem"`
}

func (r *RegistroDetalhePedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhePedido

	err = posicaoParaValor.ReturnByType(&r.TipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.CnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.SequenciaDoItem, "sequenciaDoItem")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoProduto, "codigoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.Quatidade, "quatidade")
	err = posicaoParaValor.ReturnByType(&r.Desconto, "desconto")
	err = posicaoParaValor.ReturnByType(&r.ValorUnitarioDoItem, "valorUnitarioDoItem")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalDoItem, "valorTotalDoItem")
	err = posicaoParaValor.ReturnByType(&r.TipoDoItem, "tipoDoItem")

	return err
}

var PosicoesRegistroDetalhePedido = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":      {0, 2, 0},
	"cnpjDoCliente":       {3, 17, 0},
	"numeroDoPedido":      {18, 30, 0},
	"sequenciaDoItem":     {31, 36, 0},
	"codigoDoProduto":     {37, 50, 0},
	"quatidade":           {51, 57, 0},
	"desconto":            {58, 63, 2},
	"valorUnitarioDoItem": {64, 76, 2},
	"valorTotalDoItem":    {77, 89, 2},
	"tipoDoItem":          {90, 91, 0},
}
