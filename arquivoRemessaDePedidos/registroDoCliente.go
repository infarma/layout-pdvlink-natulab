package arquivoRemessaDePedidos

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
)

type RegistroDoCliente struct {
	tipoDoRegistro                     int   	`json:"tipoDoRegistro"`
	cnpjDoCliente                      string	`json:"cnpjDoCliente"`
	nome                               string	`json:"nome"`
	nomeFantasia                       string	`json:"nomeFantasia"`
	endereco                           string	`json:"endereco"`
	bairro                             string	`json:"bairro"`
	cidade                             string	`json:"cidade"`
	estado                             string	`json:"estado"`
	cep                                string	`json:"cep"`
	telefone                           string	`json:"telefone"`
	contato                            string	`json:"contato"`
	cargoContato                       string	`json:"cargoContato"`
	email                              string	`json:"email"`
	dataDeCadastro                     int32	`json:"dataDeCadastro"`
	sugestaoDeLimiteDeCreditoCadastral float32	`json:"sugestaoDeLimiteDeCreditoCadastral"`
	vencimentoDoLimiteDeCredito        int32	`json:"vencimentoDoLimiteDeCredito"`
	inscricaoEstadual                  string	`json:"inscricaoEstadual"`
	suframa                            string	`json:"suframa"`
	observacaoDoCliente                string	`json:"observacaoDoCliente"`
	observacaoDaNotaFiscalDoCliente    string	`json:"observacaoDaNotaFiscalDoCliente"`
}

func (r *RegistroDoCliente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDoCliente

	err = posicaoParaValor.ReturnByType(&r.tipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.nome, "nome")
	err = posicaoParaValor.ReturnByType(&r.nomeFantasia, "nomeFantasia")
	err = posicaoParaValor.ReturnByType(&r.endereco, "endereco")
	err = posicaoParaValor.ReturnByType(&r.bairro, "bairro")
	err = posicaoParaValor.ReturnByType(&r.cidade, "cidade")
	err = posicaoParaValor.ReturnByType(&r.estado, "estado")
	err = posicaoParaValor.ReturnByType(&r.cep, "cep")
	err = posicaoParaValor.ReturnByType(&r.telefone, "telefone")
	err = posicaoParaValor.ReturnByType(&r.contato, "contato")
	err = posicaoParaValor.ReturnByType(&r.cargoContato, "cargoContato")
	err = posicaoParaValor.ReturnByType(&r.email, "email")
	err = posicaoParaValor.ReturnByType(&r.dataDeCadastro, "dataDeCadastro")
	err = posicaoParaValor.ReturnByType(&r.sugestaoDeLimiteDeCreditoCadastral, "sugestaoDeLimiteDeCreditoCadastral")
	err = posicaoParaValor.ReturnByType(&r.vencimentoDoLimiteDeCredito, "vencimentoDoLimiteDeCredito")
	err = posicaoParaValor.ReturnByType(&r.inscricaoEstadual, "inscricaoEstadual")
	err = posicaoParaValor.ReturnByType(&r.suframa, "suframa")
	err = posicaoParaValor.ReturnByType(&r.observacaoDoCliente, "observacaoDoCliente")
	err = posicaoParaValor.ReturnByType(&r.observacaoDaNotaFiscalDoCliente, "observacaoDaNotaFiscalDoCliente")

	return err
}

var PosicoesRegistroDoCliente = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":                     {0, 2, 0},
	"cnpjDoCliente":                      {3, 17, 0},
	"nome":                               {18, 78, 0},
	"nomeFantasia":                       {79, 109, 0},
	"endereco":                           {110, 170, 0},
	"bairro":                             {171, 231, 0},
	"cidade":                             {232, 292, 0},
	"estado":                             {293, 295, 0},
	"cep":                                {296, 305, 0},
	"telefone":                           {306, 360, 0},
	"contato":                            {361, 411, 0},
	"cargoContato":                       {412, 462, 0},
	"email":                              {463, 563, 0},
	"dataDeCadastro":                     {564, 572, 0},
	"sugestaoDeLimiteDeCreditoCadastral": {573, 585, 2},
	"vencimentoDoLimiteDeCredito":        {586, 594, 0},
	"inscricaoEstadual":                  {595, 615, 0},
	"suframa":                            {616, 636, 0},
	"observacaoDoCliente":                {637, 1137, 0},
	"observacaoDaNotaFiscalDoCliente":    {1138, 1393, 0},
}


