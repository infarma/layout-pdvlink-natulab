package arquivoRemessaDePedidos

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
)

type RegistroHeaderIdentificacaoDistribuidor struct {
	tipoDeRegistro      int   	`json:"tipoDeRegistro"`
	cnpjDoDistribuidor  string	`json:"cnpjDoDistribuidor"`
	dataDoProcessamento string	`json:"dataDoProcessamento"`
	horaDoProcessamento string	`json:"horaDoProcessamento"`
}

func (r *RegistroHeaderIdentificacaoDistribuidor) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroHeaderIdentificacaoDistribuidor

	err = posicaoParaValor.ReturnByType(&r.tipoDeRegistro, "tipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoDistribuidor, "cnpjDoDistribuidor")
	err = posicaoParaValor.ReturnByType(&r.dataDoProcessamento, "dataDoProcessamento")
	err = posicaoParaValor.ReturnByType(&r.horaDoProcessamento, "horaDoProcessamento")

	return err
}

var PosicoesRegistroHeaderIdentificacaoDistribuidor = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDeRegistro":      {0, 2,0},
	"cnpjDoDistribuidor": {3, 17,0},
	"dataDoProcessamento": {18, 26,0},
	"horaDoProcessamento": {27, 33,0},
}