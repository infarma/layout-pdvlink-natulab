package arquivoRemessaDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroHeaderPedido struct {
	TipoDoRegistro          int     `json:"TipoDoRegistro"`
	CnpjDoCliente           string  `json:"CnpjDoCliente"`
	NumeroDoPedido          string  `json:"NumeroDoPedido"`
	DataDoPedido            string  `json:"DataDoPedido"`
	PrazoDePagamento        int     `json:"PrazoDePagamento"`
	NumeroDoPedidoDoCliente string  `json:"NumeroDoPedidoDoCliente"`
	TotalDoPedido           float32 `json:"TotalDoPedido"`
}

func (r *RegistroHeaderPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroHeaderPedido

	err = posicaoParaValor.ReturnByType(&r.TipoDoRegistro, "tipoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.CnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.DataDoPedido, "dataDoPedido")
	err = posicaoParaValor.ReturnByType(&r.PrazoDePagamento, "prazoDePagamento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedidoDoCliente, "numeroDoPedidoDoCliente")
	err = posicaoParaValor.ReturnByType(&r.TotalDoPedido, "totalDoPedido")

	return err
}

var PosicoesRegistroHeaderPedido = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDoRegistro":          {0, 2, 0},
	"cnpjDoCliente":           {3, 17, 0},
	"numeroDoPedido":          {18, 30, 0},
	"dataDoPedido":            {31, 39, 0},
	"prazoDePagamento":        {40, 100, 0},
	"numeroDoPedidoDoCliente": {101, 121, 0},
	"totalDoPedido":           {122, 134, 2},
}
