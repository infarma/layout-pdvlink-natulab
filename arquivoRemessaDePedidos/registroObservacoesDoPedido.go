package arquivoRemessaDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroObservacoesDoPedido struct {
	tipoDeRegistro            int32 	`json:"tipoDeRegistro"`
	cnpjDoCliente             string	`json:"cnpjDoCliente"`
	numeroDoPedido            string	`json:"numeroDoPedido"`
	numeroDaLinhaDaObservacao int32   	`json:"numeroDaLinhaDaObservacao"`
	mensagemDoPedido          string	`json:"mensagemDoPedido"`
	observacaoDaNotaFiscal    string	`json:"observacaoDaNotaFiscal"`
	observacao                string	`json:"observacao"`
}

func (r *RegistroObservacoesDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroObservacoesDoPedido

	err = posicaoParaValor.ReturnByType(&r.tipoDeRegistro, "tipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.cnpjDoCliente, "cnpjDoCliente")
	err = posicaoParaValor.ReturnByType(&r.numeroDoPedido, "numeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.numeroDaLinhaDaObservacao, "numeroDaLinhaDaObservacao")
	err = posicaoParaValor.ReturnByType(&r.mensagemDoPedido, "mensagemDoPedido")
	err = posicaoParaValor.ReturnByType(&r.observacaoDaNotaFiscal, "observacaoDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.observacao, "observacao")

	return err
}

var PosicoesRegistroObservacoesDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"tipoDeRegistro":                  {0, 2, 0},
	"cnpjDoCliente":                   {3, 17, 0},
	"numeroDoPedido":                  {18, 30, 0},
	"numeroDaLinhaDaObservacao":       {31, 33, 0},
	"mensagemDoPedido":                {34, 35, 0},
	"observacaoDaNotaFiscal":          {36, 37, 0},
	"observacao":                      {38, 138, 0},
}