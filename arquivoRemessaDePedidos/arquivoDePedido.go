package arquivoRemessaDePedidos

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	RegistroDetalhePedido                   RegistroDetalhePedido                   `json:"RegistroDetalhePedido"`
	RegistroDoCliente                       RegistroDoCliente                       `json:"RegistroDoCliente"`
	RegistroHeaderIdentificacaoDistribuidor RegistroHeaderIdentificacaoDistribuidor `json:"RegistroHeaderIdentificacaoDistribuidor"`
	RegistroHeaderPedido                    RegistroHeaderPedido                    `json:"RegistroHeaderPedido"`
	RegistroObservacoesDoPedido             RegistroObservacoesDoPedido             `json:"RegistroObservacoesDoPedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		//TODO: Avaliar a composição dos itens do pedido
		if identificador == "1" {
			err = arquivo.RegistroDetalhePedido.ComposeStruct(string(runes))
		} else if identificador == "2" {
				err = arquivo.RegistroDoCliente.ComposeStruct(string(runes))
		} else if identificador == "3" {
			err = arquivo.RegistroHeaderIdentificacaoDistribuidor.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.RegistroHeaderPedido.ComposeStruct(string(runes))
		} else if identificador == "5" {
			err = arquivo.RegistroObservacoesDoPedido.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
